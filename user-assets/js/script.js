$(document).ready(function() {
    $('.collapse-btn').click(function() {
        if ($(this).hasClass('collapsed')) {
            $(this).closest('.card-body').find('.plus-minus img').attr('src', 'assets/images/ph_plus-circle-fill.jpg');
        } else {
            $(this).closest('.card-body').find('.plus-minus img').attr('src', 'assets/images/ph_minus-circle-fill.jpg');
        }
    });

});


$('#owl-carousel-1').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    loop: true,
    margin: 40,
    responsiveClass: true,
    nav: true, // Enable navigation
    navText: ["<i class='text-light fa fa-chevron-left'></i>", "<i class='text-light fa fa-chevron-right'></i>"], // Left and right arrow icons
    stagePadding: 100,
    responsive: {
        0: {
            items: 1
        },
        568: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 3
        }
    }
});


$('#owl-carousel-2').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    loop: true,
    margin: 40,
    responsiveClass: true,
    nav: true, // Enable navigation
    navText: ["<i class='text-light fa fa-chevron-left'></i>", "<i class='text-light fa fa-chevron-right'></i>"], // Left and right arrow icons
    stagePadding: 100,
    responsive: {
        0: {
            items: 1
        },
        568: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 3
        }
    }
});